<?php
/**
 * Congratulations.
 * If you've found this file it means you're on the right track.
 */

class Util {
  function setup() {
    echo "Producing cards: \n";
    $list = FALSE;
    if ( @$_POST['list'] )
      $list = $_POST['list'];
    elseif ( @$GLOBALS['argv'][1] )
      $list = implode( ',', array_splice($GLOBALS['argv'], 1) );
    echo "$list\n";
    if ( !$list )
      return FALSE;
    if ( !preg_match( '/^(\d+,?\s?)*$/', $list ) )
      return FALSE;

    $sql =
      "SELECT
        d.`cards_id`,
        o.`customer_id`,
        oi.`qty`                          AS `card_qty`,
        d.`card_type`,
        o.`increment_id`,
        o.`customer_lastname`                           AS `user_last_name`,
        o.`customer_firstname`                          AS `user_first_name`
      FROM `shop_orders_items` oi
        INNER JOIN `cards_info`                d  ON (d.`cards_id`  = oi.`cards_id`)
        INNER JOIN `sales_orders`               o  ON (o.`increment_id`  = oi.`increment_id`)

      WHERE 1=1
      AND oi.`cards_id`        IN($list)
      AND d.production_date != '0000-00-00'

      ORDER BY
        oi.`cards_id`
    ";

    $dbh = new PDO('mysql:host=localhost;dbname=programmer_test;charset=utf8', 'root', '');
    $sth = $dbh->prepare( $sql );
    $sth->execute();

    /* Fetch all of the remaining rows in the result set */
    print("Fetch all of the remaining rows in the result set:\n");
    $result = $sth->fetchAll(PDO::FETCH_OBJ);
    print_r($result);
  }
  /**
   * Produce cards
   */
  function produce( $cards ) {
    foreach ( $cards as $card_id ) {
      $card = self::get_card( $card_id );
      Util::generate_pdf( $card );
      Util::add_to_csv( $card );
    }
  }
  /**
   * A dummy function for generating PDF's
   * If this function is triggered it means the PDF would have been created.
   * I have not implmented real PDF generation for this task.
   */
  function generate_pdf( $card ) {
    if ( !is_object( $card ) ) {
      return FALSE;
    }
    echo "Success! PDF for order #$card->increment_id has been generated!";

  }
}
Util::setup();
class BatchUpdate
{
  public function __construct($process_list)
  {
    // @payment_rules
    $this->query = "";

    $res = DB::query($this->query);

    while ($rec = $res->fetchObject())
    {
      $this->addItem(new BatchItem(
        $rec->user_id,
        $rec->doc_id,
        $rec->doc_type,
        $rec->doc_qty,
        $rec->school_id,
        $rec->school_name,
        $rec->box_id,
        $rec->class_name,
        $rec->user_first_name,
        $rec->user_last_name,
        $rec->order_id
      ));
    }
  }
}



