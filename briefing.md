# Programming skill test

> I have a list of orders for wedding invitations I'd like to send to production.
> Here's the list of orders:
> 747, 1112, 1435, 1747, 1883
> 
> Please send me the PDF's asap!
>
> --
> Cheers,
> Customer X

## Task:

Somewhere in the code there's a file that will produce the cards.
Find it and use it to produce the cards.
You need to setup the database and correct the connection details in the file for it to work.

I haven't made a PDF generator in this example code.
Just send the debug data for the cards that should be produced.

In the report you should write the following:
* The data for the cards that are going to be sent to production.
* If you should find any errors in the code, fix them and write in the report what you had to change.
* Time spent on the task.
* Any questions you would ask the customer.

Please be concise in your report.

## Additional Information:

This task is heavily inspired by an issue we were presented by a customer and very close to a real-life scenario.
The files inside the code directory is a mashup of different content management systems.
The purpose of the files are simply to act as a camouflage for the production file.

Inside the programmer_test.sql there are lot of tables from a CMS, only a few of the tables in the database are in use for this task.

This task is meant to be complicated and dificult to solve, excpect to use several hours to complete it.
